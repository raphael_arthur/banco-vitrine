-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: vitrine
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categoria`
--

DROP TABLE IF EXISTS `categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria` (
  `idcategoria` int(11) NOT NULL AUTO_INCREMENT,
  `nome_categoria` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idcategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria`
--

LOCK TABLES `categoria` WRITE;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` VALUES (2,'Software'),(3,'Saude'),(4,'Engenharia'),(5,'Biologia'),(6,'Quimica'),(7,'Agronomia'),(8,'GestÃ£o');
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departamento`
--

DROP TABLE IF EXISTS `departamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departamento` (
  `iddepartamento` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `sigla` varchar(45) NOT NULL,
  PRIMARY KEY (`iddepartamento`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departamento`
--

LOCK TABLES `departamento` WRITE;
/*!40000 ALTER TABLE `departamento` DISABLE KEYS */;
INSERT INTO `departamento` VALUES (3,'DEP_C','C'),(4,'DEP_A','A');
/*!40000 ALTER TABLE `departamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventor`
--

DROP TABLE IF EXISTS `inventor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventor` (
  `idinventor` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `lattes` longtext NOT NULL,
  PRIMARY KEY (`idinventor`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventor`
--

LOCK TABLES `inventor` WRITE;
/*!40000 ALTER TABLE `inventor` DISABLE KEYS */;
INSERT INTO `inventor` VALUES (1,'Mariana Bernardes','http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4710452T8'),(2,'Geovany Borges','http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4798115U8'),(3,'ThÃ©rÃ¨se Hofmann Gatti Rodrigues da Costa','http://lattes.cnpq.br/6716704101303638');
/*!40000 ALTER TABLE `inventor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `laboratorio`
--

DROP TABLE IF EXISTS `laboratorio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `laboratorio` (
  `idlaboratorio` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `descricao` longtext NOT NULL,
  `sigla` varchar(45) NOT NULL,
  `servico_prestado` longtext NOT NULL,
  `coord_nome` varchar(45) NOT NULL,
  `coord_link` varchar(45) NOT NULL,
  `usuario_idusuario` int(11) NOT NULL,
  `departamento_iddepartamento` int(11) NOT NULL,
  PRIMARY KEY (`idlaboratorio`,`usuario_idusuario`,`departamento_iddepartamento`),
  KEY `fk_laboratorio_usuario1_idx` (`usuario_idusuario`),
  KEY `fk_laboratorio_departamento1_idx` (`departamento_iddepartamento`),
  CONSTRAINT `fk_laboratorio_departamento1` FOREIGN KEY (`departamento_iddepartamento`) REFERENCES `departamento` (`iddepartamento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_laboratorio_usuario1` FOREIGN KEY (`usuario_idusuario`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `laboratorio`
--

LOCK TABLES `laboratorio` WRITE;
/*!40000 ALTER TABLE `laboratorio` DISABLE KEYS */;
INSERT INTO `laboratorio` VALUES (2,'Lab1','desc do lab1','L1','serviÃ§o prestado pelo lab1','Jose','www.google.com.br',3,3);
/*!40000 ALTER TABLE `laboratorio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `palavra_chave`
--

DROP TABLE IF EXISTS `palavra_chave`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `palavra_chave` (
  `idpalavra_chave` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `categoria_idcategoria` int(11) NOT NULL,
  PRIMARY KEY (`idpalavra_chave`,`categoria_idcategoria`),
  KEY `fk_palavra_chave_categoria1_idx` (`categoria_idcategoria`),
  CONSTRAINT `fk_palavra_chave_categoria1` FOREIGN KEY (`categoria_idcategoria`) REFERENCES `categoria` (`idcategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `palavra_chave`
--

LOCK TABLES `palavra_chave` WRITE;
/*!40000 ALTER TABLE `palavra_chave` DISABLE KEYS */;
INSERT INTO `palavra_chave` VALUES (3,'Ionic2',2),(4,'Angular',2),(5,'Vacina',3),(6,'Reaproveitamento',6);
/*!40000 ALTER TABLE `palavra_chave` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `palavra_chave_has_laboratorio`
--

DROP TABLE IF EXISTS `palavra_chave_has_laboratorio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `palavra_chave_has_laboratorio` (
  `palavra_chave_idpalavra_chave` int(11) NOT NULL,
  `laboratorio_idlaboratorio` int(11) NOT NULL,
  PRIMARY KEY (`palavra_chave_idpalavra_chave`,`laboratorio_idlaboratorio`),
  KEY `fk_palavra_chave_has_laboratorio_laboratorio1_idx` (`laboratorio_idlaboratorio`),
  KEY `fk_palavra_chave_has_laboratorio_palavra_chave1_idx` (`palavra_chave_idpalavra_chave`),
  CONSTRAINT `fk_palavra_chave_has_laboratorio_laboratorio1` FOREIGN KEY (`laboratorio_idlaboratorio`) REFERENCES `laboratorio` (`idlaboratorio`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_palavra_chave_has_laboratorio_palavra_chave1` FOREIGN KEY (`palavra_chave_idpalavra_chave`) REFERENCES `palavra_chave` (`idpalavra_chave`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `palavra_chave_has_laboratorio`
--

LOCK TABLES `palavra_chave_has_laboratorio` WRITE;
/*!40000 ALTER TABLE `palavra_chave_has_laboratorio` DISABLE KEYS */;
INSERT INTO `palavra_chave_has_laboratorio` VALUES (3,2);
/*!40000 ALTER TABLE `palavra_chave_has_laboratorio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projeto`
--

DROP TABLE IF EXISTS `projeto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projeto` (
  `idprojeto` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `usuario_idusuario` int(11) NOT NULL,
  PRIMARY KEY (`idprojeto`,`usuario_idusuario`),
  KEY `fk_projeto_usuario1_idx` (`usuario_idusuario`),
  CONSTRAINT `fk_projeto_usuario1` FOREIGN KEY (`usuario_idusuario`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projeto`
--

LOCK TABLES `projeto` WRITE;
/*!40000 ALTER TABLE `projeto` DISABLE KEYS */;
/*!40000 ALTER TABLE `projeto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projeto_has_laboratorio`
--

DROP TABLE IF EXISTS `projeto_has_laboratorio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projeto_has_laboratorio` (
  `projeto_idprojeto` int(11) NOT NULL,
  `laboratorio_idlaboratorio` int(11) NOT NULL,
  PRIMARY KEY (`projeto_idprojeto`,`laboratorio_idlaboratorio`),
  KEY `fk_projeto_has_laboratorio_laboratorio1_idx` (`laboratorio_idlaboratorio`),
  KEY `fk_projeto_has_laboratorio_projeto1_idx` (`projeto_idprojeto`),
  CONSTRAINT `fk_projeto_has_laboratorio_laboratorio1` FOREIGN KEY (`laboratorio_idlaboratorio`) REFERENCES `laboratorio` (`idlaboratorio`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_projeto_has_laboratorio_projeto1` FOREIGN KEY (`projeto_idprojeto`) REFERENCES `projeto` (`idprojeto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projeto_has_laboratorio`
--

LOCK TABLES `projeto_has_laboratorio` WRITE;
/*!40000 ALTER TABLE `projeto_has_laboratorio` DISABLE KEYS */;
/*!40000 ALTER TABLE `projeto_has_laboratorio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projeto_has_palavra_chave`
--

DROP TABLE IF EXISTS `projeto_has_palavra_chave`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projeto_has_palavra_chave` (
  `projeto_idprojeto` int(11) NOT NULL,
  `palavra_chave_idpalavra_chave` int(11) NOT NULL,
  PRIMARY KEY (`projeto_idprojeto`,`palavra_chave_idpalavra_chave`),
  KEY `fk_projeto_has_palavra_chave_palavra_chave1_idx` (`palavra_chave_idpalavra_chave`),
  KEY `fk_projeto_has_palavra_chave_projeto1_idx` (`projeto_idprojeto`),
  CONSTRAINT `fk_projeto_has_palavra_chave_palavra_chave1` FOREIGN KEY (`palavra_chave_idpalavra_chave`) REFERENCES `palavra_chave` (`idpalavra_chave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_projeto_has_palavra_chave_projeto1` FOREIGN KEY (`projeto_idprojeto`) REFERENCES `projeto` (`idprojeto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projeto_has_palavra_chave`
--

LOCK TABLES `projeto_has_palavra_chave` WRITE;
/*!40000 ALTER TABLE `projeto_has_palavra_chave` DISABLE KEYS */;
/*!40000 ALTER TABLE `projeto_has_palavra_chave` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `setor`
--

DROP TABLE IF EXISTS `setor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `setor` (
  `idsetor` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idsetor`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `setor`
--

LOCK TABLES `setor` WRITE;
/*!40000 ALTER TABLE `setor` DISABLE KEYS */;
INSERT INTO `setor` VALUES (1,'AB'),(2,'B'),(3,'');
/*!40000 ALTER TABLE `setor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tecnologia`
--

DROP TABLE IF EXISTS `tecnologia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tecnologia` (
  `idtecnologia` int(11) NOT NULL AUTO_INCREMENT,
  `nome` longtext,
  `descricao` longtext NOT NULL,
  `dados_protecao` longtext,
  `usuario_idusuario` int(11) NOT NULL,
  `projeto_idprojeto` int(11) DEFAULT NULL,
  `apelido` varchar(45) DEFAULT NULL,
  `situacao` varchar(45) DEFAULT NULL,
  `resumo` longtext,
  `vantagem` longtext,
  `licenciada` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idtecnologia`),
  KEY `fk_tecnologia_usuario1_idx` (`usuario_idusuario`),
  KEY `fk_tecnologia_projeto1_idx` (`projeto_idprojeto`),
  CONSTRAINT `fk_tecnologia_projeto1` FOREIGN KEY (`projeto_idprojeto`) REFERENCES `projeto` (`idprojeto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tecnologia_usuario1` FOREIGN KEY (`usuario_idusuario`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tecnologia`
--

LOCK TABLES `tecnologia` WRITE;
/*!40000 ALTER TABLE `tecnologia` DISABLE KEYS */;
INSERT INTO `tecnologia` VALUES (1,'Tech1','descTech1','dadosTech1',3,NULL,'T1 apelido','anuidade','resumo da tech1','vantagem1 tech1. Vantagem 2;','LICENCIADA TAL TAL'),(2,'Tech2','desc da tech 2','asdflkj',3,NULL,'T2 APELIDO','anuidade','resumo tech2','vantagem blablabla','licenciada tal tal'),(3,'uoa','desc uoa','asdf',3,NULL,'Teste','xxxx','resumo de uaoaoa','vantagem 1 2 3 4','licenciada tal tal'),(4,'asdf','asdf','asdf',3,NULL,'asdf','asdf','asdf','asdf','3'),(5,'Reaproveitamento de Fibras de Acetato de Celulose e Filtros de Cigarro para ObtenÃ§Ã£o de Celulose e Papel','TECNOLOGIA INOVADORA PARA REAPROVEITAMENTO DE PAPEL E FILTRO DE CIGARRO A perspectiva do aproveitamento dos filtros de cigarro vem ao encontro da possibilidade de utilizar os materiais, atÃ© entÃ£o descartados no meio ambiente, diminuir a emissÃ£o de resÃ­duos sÃ³lidos e gerar uma atividade econÃ´mica. A tecnologia em questÃ£o inova no tratamento e processo de reciclagem de filtro de cigarro. A alternativa apresentada aqui se refere Ã  obtenÃ§Ã£o de celulose a partir de filtros de cigarro por meio de hidrÃ³lise acelerada. A inovaÃ§Ã£o tambÃ©m faz menÃ§Ã£o ao aproveitamento de resÃ­duos compostos por fibras de acetato de celulose oriundas de sobras industriais (sobras ou descartes) ou de materiais consumidos filtros de cigarro, cargas de canetas, entre outros). Nessa tecnologia, estes materiais sÃ£o usados para a obtenÃ§Ã£o de uma massa de celulose para produÃ§Ã£o de papel e outros artigos de papelaria.','Patente de InvenÃ§Ã£o  PI 0305004-1 A  DepÃ³sito: 06/10/2003; ConcessÃ£o: 11/11/2014.',3,NULL,'Bituca de cigarro','Anuidade','Reciclagem de fibras de acetato de celulose e filtros de cigarro para obtenÃ§Ã£o de celulose e papel.','â€¢ Produto ecologicamente correto; â€¢ Processo inovador; â€¢ Beneficiamento de um microlixo; â€¢ Papel resistente; â€¢ GeraÃ§Ã£o de empregos; â€¢ Tecnologia social nacional; â€¢ DiminuiÃ§Ã£o significativa de poluiÃ§Ã£o do solo.','3');
/*!40000 ALTER TABLE `tecnologia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tecnologia_has_departamento`
--

DROP TABLE IF EXISTS `tecnologia_has_departamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tecnologia_has_departamento` (
  `tecnologia_idtecnologia` int(11) NOT NULL,
  `departamento_iddepartamento` int(11) NOT NULL,
  PRIMARY KEY (`tecnologia_idtecnologia`,`departamento_iddepartamento`),
  KEY `fk_tecnologia_has_departamento_departamento1_idx` (`departamento_iddepartamento`),
  KEY `fk_tecnologia_has_departamento_tecnologia1_idx` (`tecnologia_idtecnologia`),
  CONSTRAINT `fk_tecnologia_has_departamento_departamento1` FOREIGN KEY (`departamento_iddepartamento`) REFERENCES `departamento` (`iddepartamento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tecnologia_has_departamento_tecnologia1` FOREIGN KEY (`tecnologia_idtecnologia`) REFERENCES `tecnologia` (`idtecnologia`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tecnologia_has_departamento`
--

LOCK TABLES `tecnologia_has_departamento` WRITE;
/*!40000 ALTER TABLE `tecnologia_has_departamento` DISABLE KEYS */;
INSERT INTO `tecnologia_has_departamento` VALUES (1,3),(2,3),(3,4);
/*!40000 ALTER TABLE `tecnologia_has_departamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tecnologia_has_inventor`
--

DROP TABLE IF EXISTS `tecnologia_has_inventor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tecnologia_has_inventor` (
  `tecnologia_idtecnologia` int(11) NOT NULL,
  `inventor_idinventor` int(11) NOT NULL,
  PRIMARY KEY (`tecnologia_idtecnologia`,`inventor_idinventor`),
  KEY `fk_tecnologia_has_inventor_inventor1_idx` (`inventor_idinventor`),
  KEY `fk_tecnologia_has_inventor_tecnologia1_idx` (`tecnologia_idtecnologia`),
  CONSTRAINT `fk_tecnologia_has_inventor_inventor1` FOREIGN KEY (`inventor_idinventor`) REFERENCES `inventor` (`idinventor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tecnologia_has_inventor_tecnologia1` FOREIGN KEY (`tecnologia_idtecnologia`) REFERENCES `tecnologia` (`idtecnologia`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tecnologia_has_inventor`
--

LOCK TABLES `tecnologia_has_inventor` WRITE;
/*!40000 ALTER TABLE `tecnologia_has_inventor` DISABLE KEYS */;
INSERT INTO `tecnologia_has_inventor` VALUES (1,1),(2,1),(3,2),(4,3),(5,3);
/*!40000 ALTER TABLE `tecnologia_has_inventor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tecnologia_has_laboratorio`
--

DROP TABLE IF EXISTS `tecnologia_has_laboratorio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tecnologia_has_laboratorio` (
  `tecnologia_idtecnologia` int(11) NOT NULL,
  `laboratorio_idlaboratorio` int(11) NOT NULL,
  PRIMARY KEY (`tecnologia_idtecnologia`,`laboratorio_idlaboratorio`),
  KEY `fk_tecnologia_has_laboratorio_laboratorio1_idx` (`laboratorio_idlaboratorio`),
  KEY `fk_tecnologia_has_laboratorio_tecnologia1_idx` (`tecnologia_idtecnologia`),
  CONSTRAINT `fk_tecnologia_has_laboratorio_laboratorio1` FOREIGN KEY (`laboratorio_idlaboratorio`) REFERENCES `laboratorio` (`idlaboratorio`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tecnologia_has_laboratorio_tecnologia1` FOREIGN KEY (`tecnologia_idtecnologia`) REFERENCES `tecnologia` (`idtecnologia`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tecnologia_has_laboratorio`
--

LOCK TABLES `tecnologia_has_laboratorio` WRITE;
/*!40000 ALTER TABLE `tecnologia_has_laboratorio` DISABLE KEYS */;
INSERT INTO `tecnologia_has_laboratorio` VALUES (1,2),(2,2),(3,2);
/*!40000 ALTER TABLE `tecnologia_has_laboratorio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tecnologia_has_palavra_chave`
--

DROP TABLE IF EXISTS `tecnologia_has_palavra_chave`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tecnologia_has_palavra_chave` (
  `tecnologia_idtecnologia` int(11) NOT NULL,
  `palavra_chave_idpalavra_chave` int(11) NOT NULL,
  PRIMARY KEY (`tecnologia_idtecnologia`,`palavra_chave_idpalavra_chave`),
  KEY `fk_tecnologia_has_palavra_chave_palavra_chave1_idx` (`palavra_chave_idpalavra_chave`),
  KEY `fk_tecnologia_has_palavra_chave_tecnologia1_idx` (`tecnologia_idtecnologia`),
  CONSTRAINT `fk_tecnologia_has_palavra_chave_palavra_chave1` FOREIGN KEY (`palavra_chave_idpalavra_chave`) REFERENCES `palavra_chave` (`idpalavra_chave`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tecnologia_has_palavra_chave_tecnologia1` FOREIGN KEY (`tecnologia_idtecnologia`) REFERENCES `tecnologia` (`idtecnologia`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tecnologia_has_palavra_chave`
--

LOCK TABLES `tecnologia_has_palavra_chave` WRITE;
/*!40000 ALTER TABLE `tecnologia_has_palavra_chave` DISABLE KEYS */;
INSERT INTO `tecnologia_has_palavra_chave` VALUES (1,3),(2,3),(3,5),(4,5),(5,6);
/*!40000 ALTER TABLE `tecnologia_has_palavra_chave` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `sobrenome` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `senha` varchar(45) NOT NULL,
  `ramal` varchar(45) NOT NULL,
  `admin` tinyint(1) DEFAULT '0',
  `admin_act` tinyint(1) DEFAULT '0',
  `user_act` tinyint(1) DEFAULT '0',
  `admin_projeto` tinyint(1) DEFAULT '0',
  `user_projeto` tinyint(1) DEFAULT NULL,
  `setor_idsetor` int(11) NOT NULL,
  `tentativas` int(11) DEFAULT '0',
  PRIMARY KEY (`idusuario`,`setor_idsetor`),
  KEY `fk_usuario_setor_idx` (`setor_idsetor`),
  CONSTRAINT `fk_usuario_setor` FOREIGN KEY (`setor_idsetor`) REFERENCES `setor` (`idsetor`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (3,'Raphael','Resende','raphael.arthur@hotmail.com','$1$i9X/UWeC$5eSPebQPOLKSLsnVNGs/T0','4103',1,0,0,0,0,1,0);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-29  8:58:45
